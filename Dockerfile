FROM debian:buster

RUN apt-get update && apt-get install -y wget gnupg unzip apt-transport-https && rm -rf /var/lib/apt/lists/*

ENV TF_VERSION 0.12.4
# Verify this is the same key listed at https://www.hashicorp.com/security.html
RUN gpg --recv-keys 91A6E7F85D05C65630BEF18951852D87348FFC4C
RUN wget https://releases.hashicorp.com/terraform/${TF_VERSION}/terraform_${TF_VERSION}_SHA256SUMS
RUN wget https://releases.hashicorp.com/terraform/${TF_VERSION}/terraform_${TF_VERSION}_SHA256SUMS.sig
RUN gpg --verify terraform_${TF_VERSION}_SHA256SUMS.sig
RUN wget https://releases.hashicorp.com/terraform/${TF_VERSION}/terraform_${TF_VERSION}_linux_amd64.zip
RUN sha256sum -c --ignore-missing terraform_${TF_VERSION}_SHA256SUMS
RUN unzip terraform_${TF_VERSION}_linux_amd64.zip
RUN mv terraform /usr/bin/terraform
RUN rm terraform_${TF_VERSION}*

RUN echo deb https://apt.kubernetes.io/ kubernetes-xenial main > /etc/apt/sources.list.d/kubernetes.list
RUN wget -O - https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
RUN apt-get update && apt-get install -y kubectl && rm -rf /var/lib/apt/lists/*
